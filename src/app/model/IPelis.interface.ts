export interface IPelis{
    title:  string;
    Genre: string;
    ImdbRating: string;
    Year: string;
    Director: string;
    Actors: string;
    Poster: string;
    Type: string;
}